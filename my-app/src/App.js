
import { Component } from "react";
import './App.css';
import Button from './components/button/button.js';
import Modal from './components/modal/modal.js';
import { modalConfigs } from "./arrModalConfigues";

class App extends Component {
  state = {
    modalIsOpen: false,
    activeModal: null,
  };

  openModal = (modalId) => {
    const modal = modalConfigs.find(modal => modal.id === modalId);
    this.setState({ modalIsOpen: true, activeModal: modal });
  };

  closeModal = () => {
    this.setState({ modalIsOpen: false, activeModal: null });
  };

  render() {
    const { modalIsOpen, activeModal } = this.state;

    return (
      <div className="App">
        <Button
          backgroundColor="violet"
          text="Open first modal"
          onClick={() => this.openModal('deleteModal')}
        />
        <Button
          backgroundColor="blue"
          text="Open second modal"
          onClick={() => this.openModal('sendModal')}
        />
        {modalIsOpen && (
          <Modal
            header={activeModal.header}
            closeButton={activeModal.closeButton}
            onClick={this.closeModal}
            text={activeModal.text}
            actions={
              <>
                <Button
                  backgroundColor="#b3382c"
                  text="Ok"
                  onClick={this.closeModal}
                />
                <Button
                  backgroundColor="#b3382c"
                  text="Cancel"
                  onClick={this.closeModal}
                />
              </>
            }
          />
        )}
      </div>
    )
  }
}

export default App;
