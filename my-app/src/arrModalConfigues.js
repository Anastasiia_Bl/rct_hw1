export const modalConfigs = [
    {
        id: 'deleteModal',
        header: "Do you want to delete this file?",
        closeButton: true,
        text: "Once you delete this file, it won`t be possible to undo this action. Are you sure you want to delete it?",
    },

    {
        id: 'sendModal',
        header: "Do you want to send this file?",
        closeButton: true,
        text: "Are you sure you want to send it? Make sure that everything is correct.",
    }
]

