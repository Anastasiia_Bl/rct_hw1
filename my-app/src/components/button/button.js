import { Component } from "react";
import "./button.scss";

class Button extends Component {
  // constructor(props) {
  //   super(props);
  // }
  render() {
    const { backgroundColor, text, onClick } = this.props;
    return (
      <button className="button__open-modal" style={{ backgroundColor }} onClick={onClick}>
        {text}
      </button>
    );
  }
}

export default Button;
